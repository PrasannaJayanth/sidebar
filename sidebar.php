<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SideBar</title>
    <link rel="stylesheet" href="./assests/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Inter:wght@500&family=Jost:wght@600&family=Poppins:wght@400;500;600&display=swap');

        body {
            font-family: sans-serif;
            color: #CCCCCC;
        }

        .nav-pills .nav-link.active {
            background-color: #fff;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row flex-nowrap">
            <div class=" col-auto col-md-2 min-vh-100" style="background-color: #003F6D; border-radius: 15px;">

                <ul class="nav nav-pills flex-column  mt-4 ">
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/Vector.png" width="18px" height="18px" style="vertical-align:top "><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-white" href="#">
                            <img src="./images/./fi_10856638.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom;  color: #111111;">Orders</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_6518335.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Dispatcher</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_2769146.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Barcodes</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_183644.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Invoices</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_3037247.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Payments</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_9813022.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Tariffs</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_5859382.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Users</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_9813022 (1).png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Expenses</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_10252880.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Promo codes</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle  text-white" href="#">
                            <img src="./images/./fi_2098758.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3" style="vertical-align:text-bottom" data-toggle="dropdown">Reports</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_3019014.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Settings</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_9001787.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Activity log</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="./images/./fi_7175236.png" width="22px" height="22px" style="vertical-align:top"><span class="d-none d-sm-inline ms-3  " style="vertical-align:text-bottom">Logout</span>
                        </a>
                    </li>


                </ul>
            </div>
        </div>
    </div>
    </div>





    <script src="./assests/js/bootstrap.min.js"></script>

</body>

</html>